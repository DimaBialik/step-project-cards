import {
    VisitCardsDentist,
    VisitCardsCardiologist,
    VisitCardsTherapist,
  } from "../classes/renderVisitCards.js";
  const checkAndFilterCard = ({
    name,
    doctor,
    description,
    urgency: priority,
    purpose: visitPurpouse,
    status,
    id,
    lastVisit,
    age,
    pressure,
    massIndex,
    diseases,
    display,
  }) => {
    if (display === "none") {
      if (doctor === "Cardiologist") {
        const newElement = new VisitCardsCardiologist(
          name,
          doctor,
          description,
          priority,
          visitPurpouse,
          status,
          id,
          age,
          pressure,
          massIndex,
          diseases
        );
        newElement.render();
      } else if (doctor === "Dentist") {
        const newElement = new VisitCardsDentist(
          name,
          doctor,
          description,
          priority,
          visitPurpouse,
          status,
          id,
          lastVisit
        );
        newElement.render();
      } else {
        const newElement = new VisitCardsTherapist(
          name,
          doctor,
          description,
          priority,
          visitPurpouse,
          status,
          id,
          age
        );
        newElement.render();
      }
    }
  };
  
  export default checkAndFilterCard;
  